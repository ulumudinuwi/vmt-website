<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->helper('phpmailer');
	}

	public function index()
	{
		$this->load->view('index');
	}

    public function send_mail() {
        $email = $this->input->post('email');

        if($email) {
            $to   = $email;

            $subject = "Donasi Masjid Ar - Rahman :)";
            $data = ['header'=>'Donasi',
                    'message'=>'<h3>Terimakasih!</h3>'];

            $message    =  $this->load->view('email',$data,true);

            if(send_email($to, $subject, $message)) {
                $this->output->set_status_header(200);
                echo json_encode(['message' => 'Berhasil! Silahkan cek email Anda']); exit();
            }
            $this->output->set_status_header(500);
            echo json_encode(['message' => 'Gagal Dikirim Silahkan periksa koneksi anda !!']);
            exit();
        }
        $this->output->set_status_header(500);
        echo json_encode(['message' => 'Akun tidak ditemukan']);
    }

}
