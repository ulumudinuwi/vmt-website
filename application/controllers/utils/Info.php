<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Display Server Info
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Info extends Admin_Controller
{

	function index()
	{
		$this->load->vars('page_title', '<i class="fa fa-wrench"></i> Info');
		$this->template->build('utils/info');
	}

	function display_info()
	{
		phpinfo();
	}

}
