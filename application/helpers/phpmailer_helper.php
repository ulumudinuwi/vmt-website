<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * PHPMailer Helper
 *
 * @package extended-CI (https://github.com/adamriyadi/CodeIgniter/)
 * @category Helper
 * @author Adam M. R (riyadi.adam@gmail.com)
 *
 */
if ( ! function_exists('send_email'))
{
    function send_email($recipient, $subject, $message)
    {
//        require_once(getcwd()."/application/third_party/PHPMailer/class.phpmailer.php");
        require_once(getcwd()."/vendor/phpmailer/phpmailer/PHPMailerAutoload.php");

        $CI =& get_instance();

        $mail = new PHPMailer();



        $body = $message;
        $mail->IsSMTP();
        $mail->Host = $CI->config->item("mail")['host'];
        $mail->SMTPAuth = true;
        $mail->Username = $CI->config->item("mail")['user'];
        $mail->Password = $CI->config->item("mail")['password'];
        $mail->Port = $CI->config->item("mail")['port'];
        $mail->SMTPSecure = $CI->config->item("mail")['smtp_secure'];
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->FromName = $CI->config->item("mail")['from'];
        $mail->From = $CI->config->item("mail")['sender'];
        $mail->Subject = $subject;
        $mail->AltBody = strip_tags($message);
        $mail->MsgHTML($body);
        $mail->AddAddress($recipient);
        if ( ! $mail->Send())
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }
}
if ( ! function_exists('send_attachment'))
{
    function send_attachment($recipient, $subject, $message, $filename)
    {
//        require_once(getcwd()."/application/third_party/PHPMailer/class.phpmailer.php");
        require_once(getcwd()."/vendor/phpmailer/phpmailer/class.phpmailer.php");

        $CI =& get_instance();


        $mail = new PHPMailer();

        $body = $message;
        $mail->IsSMTP();
        $mail->Host = $CI->config->item("mail")['host'];
        $mail->SMTPAuth = true;
        $mail->Username = $CI->config->item("mail")['user'];
        $mail->Password = $CI->config->item("mail")['password'];
        $mail->Port = $CI->config->item("mail")['port'];
        $mail->SMTPSecure = $CI->config->item("mail")['smtp_secure'];
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->FromName = $CI->config->item("mail")['from'];
        $mail->From = $CI->config->item("mail")['sender'];
        $mail->Subject = $subject;
        $mail->AltBody = strip_tags($message);
        $mail->MsgHTML($body);
        $mail->AddAddress($recipient);
        $mail->AddAttachment(getcwd()."/".$filename);
        if ( ! $mail->Send())
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }
}