<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>VMT | Home</title>
    <!-- Place favicon.ico in the root directory -->
    <!-- <link rel="apple-touch-icon" href="images/masjid/logo-masjid-1.png"> -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo base_url('assets'); ?>/images/vmt/favicon.png" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/themify-icons.css">
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/magnific-popup.css">
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/normalize.css">
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>//css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/responsive.css">
    <script src="<?php echo base_url('assets'); ?>/js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .btn-info{
            background-color: #00b386;
            border-radius: 100px;
            font-size: 14px;
            margin-top: -10px; 
        }
    </style>
</head>

<body data-spy="scroll" data-target="#primary-menu">

    <!--Mainmenu-area-->
    <div class="mainmenu-area" data-spy="affix" data-offset-top="100">
        <div class="container">
            <!--Logo-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand logo">
                    <img src="<?php echo base_url('assets'); ?>/images/vmt/logo.png" alt="" class="img-responsive" style="height: 40px !important;">
                </a>
            </div>
            <!--Logo/-->
            <nav class="collapse navbar-collapse" id="primary-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#home-page">Home</a></li>
                    <li><a href="#service-page">About Us</a></li>
                    <li><a href="#our-product">Our Products</a></li>
                    <li><a href="#team-page">Our Service</a></li>
                    <li><a href="#contact-page">Contact Us</a></li>
                    <li>
                        <a href="#">
                            <button class="btn btn-info">GET IN TOUCH NOW</button>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <!--Mainmenu-area/-->

    <!--Header-area-->
    <header class="header-area overlay full-height relative v-center" id="home-page">
        <div class="absolute anlge-bg"></div>
        <div class="container">
            <div class="row v-center">
                <div class="col-xs-12 col-md-6 header-text" style="margin-top: 150px;">
                    <table>
                        <tr>
                            <td>
                                <h1 class="ijo" style="font-size: 160px;">13</h1>
                            </td>
                            <td>
                                <h3 class="ijo" style="font-size: 40px;">Years Expirience</h3>
                                <h3 class="ijo" style="font-size: 32px;">in Software Solution</h3>
                            </td>
                        </tr>
                    </table>
                    <p style="padding: 0px; margin:0px; font-size: 18px;color: #444;">
                        Leverage Our years expetice to create a solid Software<br> 
                        foundation for your business .
                    </p>
                    <p style="margin-top: 30px;">
                        <a class="demo-btn text" href="#" style="color: #fff">View Demo</a>
                        <a class="primary-btn text" href="#" style="color: #fff">Contact Us</a>
                    </p>
                </div>
                <div class="hidden-xs hidden-sm col-md-6 text-right" style="padding-top: 12%;">
                    <img src="<?php echo base_url('assets'); ?>/images/vmt/banner.png" alt="" class="img-responsive">
                </div>
                <div class="col-md-12">
                    <p class="follow" style="color: black; margin-top: 6%;">
                        Follow Us :
                        <a href="#">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/icon-i.png" alt="" style="height: 30px;">
                        </a>
                        <a href="#">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/icon-f.png" alt="" style="height: 30px;">
                        </a>
                        <a href="#">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/icon-t.png" alt="" style="height: 30px;">
                        </a>
                        <a href="#">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/icon-y.png" alt="" style="height: 30px;">
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </header>
    <!--Header-area/-->

    <!--Feature-area-->
    <section class="section-padding" id="service-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10">
                    <div class="col-xs-12 col-sm-4" style="position: relative; z-index: 1;">
                        <img src="<?php echo base_url('assets'); ?>/images/vmt/phone.png" alt="" style="z-index: 99px;">
                    </div>
                    <div class="gray-bg col-xs-12 col-md-12" style="margin-top: 8.7%; margin-left: 27.6%; position: absolute; padding-top: 40px; padding-left: 100px; padding-bottom: 3%;">
                        <h1>PT. VMT SOFTWARE</h1>
                        <p>
                            Sed ut perspiciatis unde omnis inte natus error sit voluptateam accusaum <br>
                            doloremque laundantium, totam rem aperiam, eaque ipsa quae ab illo iventore<br>
                            veritatis et quasi architecto beatae vitae dicta sunt explicabo .
                        </p>
                        <p>
                            Sed ut perspiciatis unde omnis inte natus error sit voluptateam accusaum <br>
                            doloremque laundantium, totam rem aperiam, eaque ipsa quae ab illo iventore <br>
                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. <br>
                            Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, <br>
                            se quia consequuntur magni dolores eos qui ratione voluptatem .
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Feature-area/-->

    <section class="section-padding" id="our-product">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <h2 class="ijo">Our Products</h2>
                    <p class="text-center">
                        VMT Software Mengembangkan produk - produk<br>
                        terbaik sesuai kebutuhan Pengguna
                    </p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/12.png" alt="" style="height:50px;">
                        </div>
                        <h5>Software Rumah Sakit/Klinik</h5>
                        <p class="text-left">
                            Sistem Informasi Manajemen Rumah Sakit (SIMRS) dengan modul lengkap mulai dari front-office sampai back-office .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="https://imedis.co.id" target="_blank" class="ijo">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/14.png" alt="" style="height:50px;">
                        </div>
                        <h5>Oline Course Engine</h5>
                        <p class="text-left">
                            Online Course Software dengan fitur yang lengkap dan dapat di-custom sesuai kebutuhan .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="#" class="ijo">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/13.png" alt="" style="height:50px;">
                        </div>
                        <h5>Peer to peer (P2P) Lending Engine</h5>
                        <p class="text-left">
                            Aplikasi Pinjaman yang mempertemukan Peminjaman dengan Pemberi Pinjaman atau Investor secara online .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="#" class="ijo">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/15.png" alt="" style="height:50px;">
                        </div>
                        <h5>e-Commerce Engine</h5>
                        <p class="text-left">
                            Software belanja online dengan fitur yag lengkap dn di-custom sesuai dengan kebutuhan .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="#" class="ijo">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/16.png" alt="" style="height:50px;">
                        </div>
                        <h5>e-Govermnment</h5>
                        <p class="text-left">
                            Software yang dikembangkan untuk kebutuhan operasional atau pelayanan pada instansi pemerintahan .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="#" class="ijo">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/17.png" alt="" style="height:50px;">
                        </div>
                        <h5>MLM Software</h5>
                        <p class="text-left">
                            Multi Level Marketing (MLM) Software dengan Marketing Plan yang dapat disesuaikan dengan kebutuhan .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="#" class="ijo">Read More</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section bg-soft">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
                    <div class="col-md-3 text-center">
                        <div class="box-icon-2">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/1.png" alt="" style="height:50px;">
                        </div>
                        <span style="font-size: 16px; font-weight: 500">Software Kepegawaian</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="box-icon-2">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/2.png" alt="" style="height:50px;">
                        </div>
                        <span style="font-size: 16px; font-weight: 500">Software Aset Manajemen</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="box-icon-2">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/3.png" alt="" style="height:50px;">
                        </div>
                        <span style="font-size: 16px; font-weight: 500">Software Keuangan & Akuntansi</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="box-icon-2">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/5.png" alt="" style="height:50px;">
                        </div>
                        <span style="font-size: 16px; font-weight: 500">Software Gudang/Inventory</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="box-icon-2">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/4.png" alt="" style="height:50px;">
                        </div>
                        <span style="font-size: 16px; font-weight: 500">Software Logistik</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="box-icon-2">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/6.png" alt="" style="height:50px;">
                        </div>
                        <span style="font-size: 16px; font-weight: 500">Software Point Of Sales (POS)</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="box-icon-2">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/7.png" alt="" style="height:50px;">
                        </div>
                        <span style="font-size: 16px; font-weight: 500">Software Sales Automation</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <div class="box-icon-2">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/8.png" alt="" style="height:50px;">
                        </div>
                        <span style="font-size: 16px; font-weight: 500">Software e-Invoicing</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-padding bg-serv" id="team-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <h2>Our Services</h2>
                    <p>
                        VMT Software Memberikan layanan terbaik untuk kebutuhan<br>
                        Penggunan dengan solusi Software berbasis web dan mobile .
                    </p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="single-team">
                        <div class="text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/9.png" alt="" style="height:40px;">
                        </div>
                        <br>
                        <h4>Enterprise Software Development</h4>
                        <p class="text-left">
                            Pengembangan Software skaka enterprise yang dapat di-custom sesuai kebutuhan perusahaan .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="#" style="color: #fff;">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="single-team">
                        <div class="text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/10.png" alt="" style="height:50px;">
                        </div>
                        <br>
                        <h4>Mobile App Development</h4>
                        <p class="text-left">
                            Pengembangan Software berbasis mobile (Android & iOS) yang dapat di-custom sesuai kebutuhan perusahaan .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="#" style="color: #fff;">Read More</a>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="single-team">
                        <div class="text-center">
                            <img src="<?php echo base_url('assets'); ?>/images/vmt/11.png" alt="" style="height:50px;">
                        </div>
                        <br>
                        <h4>IT Consulting</h4>
                        <p class="text-left">
                            VMT Software juga menyediakan layanan konsultasi, pelatihan, seminar dan workshop seputar teknoligi informasi .
                        </p>
                        <p style="position: absolute; bottom: 10px; right: 20px;">
                            <a href="#" style="color: #fff;">Read More</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <aside class="aside contact-image" id="contact-page">
    <aside class="cp"></aside>
        <div class="container text-left" style="position: absolute; padding-top: 130px; padding-left: 50%;">
            <div class="row">
                <div class="col-md-12">
                    <h2>Kami Selalu Ada untuk Anda</h2>
                    <h4>
                        Kapanpun anda butuhkan kami selalu siap <br>
                        membantu ...
                    </h4>
                </div>
            </div>
        </div>
        <img src="<?php echo base_url('assets'); ?>/images/vmt/bg-cont.png" alt="" class="img-responsive" style="height: 100% !important; margin-left: 75px">
        <div class="col-md-12" style="margin-top: -5px; background-color: #797777; height: 120px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3 text-left" style="padding: 30px; vertical-align: center">
                        <table style="margin-left: 50px;">
                            <tr>
                                <td rowspan="2">
                                    <img src="<?php echo base_url('assets'); ?>/images/vmt/icon-telp.png" alt="" style="height: 30px;">
                                </td>
                                <td>
                                    <span style="font-weight: bold;font-size: 18px; color: #fff; padding-left: 20px;">No. Telp: </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-weight: bold;font-size: 18px; color: #fff; padding-left: 20px;">(022) 87773169</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3 text-left" style="padding-top: 30px;">
                        <table style="margin-left: 50px;">
                            <tr>
                                <td rowspan="2">
                                    <img src="<?php echo base_url('assets'); ?>/images/vmt/icon-email.png" alt="" style="height: 30px;">
                                </td>
                                <td>
                                    <span style="font-weight: bold;font-size: 18px; color: #fff; padding-left: 20px;">Email :</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-weight: bold;font-size: 18px; color: #fff; padding-left: 20px;">office@vmt.co.id</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6 text-left" style="padding-top: 30px;">
                        <table style="margin-left: 50px;">
                            <tr>
                                <td rowspan="2">
                                    <img src="<?php echo base_url('assets'); ?>/images/vmt/icon-alamat.png" alt="" style="height: 35px;">
                                </td>
                                <td>
                                    <span style="font-weight: bold;font-size: 18px; color: #fff; padding-left: 20px;">
                                        JL. Sunan Ambu A73, Komplek Guruminda
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-weight: bold;font-size: 18px; color: #fff; padding-left: 20px;">
                                        Soekarno Hatta - Bandung 40293
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </aside>

    <footer class="footer-area">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="<?php echo base_url('assets'); ?>/images/vmt/logo.png" alt="" style="height: 50px; !important;">
                    </div>
                </div>
                <div class="row padding">
                    <div class="col-md-7 col-sm-offset-3 text-center">
                        <nav class="collapse navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="#home-page">Home</a></li>
                                <li><a href="#service-page">About Us</a></li>
                                <li><a href="#our-product">Our Products</a></li>
                                <li><a href="#team-page">Our Service</a></li>
                                <li><a href="#contact-page">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            &copy;Copyright 2005 - <?php echo date('Y'); ?> | PT. VENUS MEDIA TEKNOLIGI |  All right reserved
                        </p>
                    </div>
                    <div class="col-md-6 text-right">
                        <span>
                            Follow us : 
                            <a href="#">
                                <i class="ti ti-instagram" style="color: white; margin-left: 10px; font-size: 20px;"></i> 
                            </a>
                            <a href="#">
                                <i class="ti ti-facebook" style="color: white; margin-left: 10px; font-size: 20px;"></i> 
                            </a>
                            <a href="#">
                                <i class="ti ti-twitter" style="color: white; margin-left: 10px; font-size: 20px;"></i> 
                            </a>
                            <a href="#">
                                <i class="ti ti-youtube" style="color: white; margin-left: 10px; font-size: 20px;"></i> 
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--Vendor-JS-->
    <script src="<?php echo base_url('assets'); ?>/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/vendor/bootstrap.min.js"></script>
    <!--Plugin-JS-->
    <script src="<?php echo base_url('assets'); ?>/js/owl.carousel.min.js"></script>
    <!-- <script src="<?php echo base_url('assets'); ?>/js/contact-form.js"></script> -->
    <script src="<?php echo base_url('assets'); ?>/js/jquery.parallax-1.1.3.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/scrollUp.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/magnific-popup.min.js"></script>
    <script src="<?php echo base_url('assets'); ?>/js/wow.min.js"></script>
    <!--Main-active-JS-->
    <script src="<?php echo base_url('assets'); ?>/js/main.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url('assets') ?>/js/validation/validate.min.js"></script>
    <script type="text/javascript">
        var form  = '#contact-form';
        //masuk
        function gow(){
            var status_proses = false;;
            if (checkSerialize_masuk() == true) {
                $(form).submit();
            } else {
                swal('Peringatan !!','Masukan Email anda .','warning');
            }
        }

        function checkSerialize_masuk(){
            var serialized = $(form).serialize();
            if(serialized.indexOf('=&') > -1 || serialized.substr(serialized.length - 1) == '='){
                return false;
            }else{
                return true;
            }
        }

        $(form).validate({
          rules: {
            email: { 
                required: true, 
                email: true 
            }
          },
          messages: {
            email: { 
                required: "Alamat Email Harus Terisi", 
                email:"Email Anda Salah"}
          },
          errorPlacement: function(error, element) {
            var parent = $(element).parent().parent();
            parent.find('.error').append(error);
          },
          submitHandler: function (form) {
            $('#masuk-btn').addClass('disabled').html('loading...');
            
            event.preventDefault();
            /* Act on the event */
            var base_url='<?php echo base_url();?>';
            var email = $('input[id=email]');
            var status = true;

            $.ajax({
                url: base_url+'index.php/home/send_mail',
                type: 'POST',
                dataType: 'json',
                data: {
                    email: email.val(),
                },
                success: function(event){
                    swal('Berhasil','Terimakasih Telah Berdonasi :)','warning');
                    location.reload();
                },
                error: function(event){
                    swal('Kesalahan','Email atau Password Salah','warning');
                }
            });
          }
        });
    </script>
</body>

</html>
